var gulp = require('gulp'),
  jade = require('gulp-jade'),
  sass = require('gulp-sass'),
  notify = require('gulp-notify'),
  plumber = require('gulp-plumber'),
  prefix = require('gulp-autoprefixer'),
  changed = require('gulp-changed'),
  bs = require('browser-sync').create();

/**
 * plugins for build
**/
var del = require('del'),
  size = require('gulp-size'),
  useref = require('gulp-useref'),
  wiredep = require('wiredep').stream;


/**
 * TASKS
**/
gulp.task('template', function () {
  return gulp.src(['./app/**/*.jade', '!./app/templates/**/*.jade'])
    .pipe(changed('.tmp'))
    .pipe(jade({
      pretty: true
    }))
    .on('error', notify.onError())
    .pipe(gulp.dest('.tmp'))
    .pipe(bs.stream({ once: true }));
});

gulp.task('styles', function () {
  return gulp.src('./app/**/*.scss')
    .pipe(changed('.tmp/styles'))
    .pipe(sass())
    .pipe(prefix({
      browsers: ['last 2 versions']
    }))
    .on('error', notify.onError())
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(bs.stream({ once: true }));
});

gulp.task('scripts', function () {
  return gulp.src(['./app/**/**/*.js'])
    .pipe(changed('.tmp'))
    .on('error', notify.onError())
    .pipe(gulp.dest('.tmp'))
    .pipe(bs.stream({ once: true }));
});

gulp.task('assets', function () {
  return gulp.src('app/images/**/*.*')
    .pipe(gulp.dest('.tmp/images'))
});


/**
 * tasks for build
**/
gulp.task('html', ['template', 'styles', 'scripts'], () => {
  return gulp.src(['.tmp/*.html', '.tmp/**/*.html'])
    .pipe(useref({ searchPath: ['.tmp', 'app', '.'] }))
    .pipe(gulp.dest('dist'));
});

// inject bower components
gulp.task('wiredep', function () {
  gulp.src('app/styles/*.scss')
    .pipe(wiredep({ ignorePath: /^(\.\.\/)+/ }))
    .pipe(gulp.dest('app/styles'));

  gulp.src(['app/*.jade', 'app/**/*.jade'])
    .pipe(wiredep({ ignorePath: /^(\.\.\/)*\.\./ }))
    .pipe(gulp.dest('app'));
});

gulp.task('extras', function () {
  return gulp.src([
    'app/**/*.*',
    '!app/*.html',
    '!app/**/*.jade',
    '!app/**/*.scss'
  ], {
      dot: true
    }).pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('build', ['html', 'assets', 'extras'], function () {
  return gulp.src('dist/**/*').pipe(size({ title: 'build', gzip: true }));
});


/**
 * watch ans serve
**/
gulp.task('watch', function () {
  gulp.watch('./app/**/*.jade', ['template']);
  gulp.watch('./app/**/*.scss', ['styles']);
  gulp.watch('./app/**/*.js', ['scripts']);
  gulp.watch('./app/images/**/*.*', ['assets']);
  gulp.watch('bower.json', ['wiredep']);
});

gulp.task('s', ['template', 'styles', 'scripts', 'assets', 'watch'], function () {
  bs.init({
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });

  bs.watch('.tmp/**/*.*').on('change', bs.reload);
});

/**
 * default task
**/
gulp.task('default', ['clean'], function () {
  gulp.start('build');
});